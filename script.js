var app = angular.module('sample', [
    'ui.router',
    'ngAnimate',
    'ngSanitize',
    'content',
    'news'
])

    .factory('utils', function(){
        return {
            findById: function findById(a, id){
                for(var i = 0; i<a.length; i++){
                    if(a[i].id == id) return a[i];
                }
                return null;
            }
        };
    })



    .factory('getJson', ['$http', 'utils', function ($http, utils){
        var path = 'db.json',
            getJson = $http.get(path)
                        .success(function(data){
                            return data;
                        });
        var factory = {};
        factory.all = function(){
            return getJson;
        };

        factory.get = function(id){
            return getJson.then(function(){
                return utils.findById(getJson, id);
            });
        };

        return factory;
    }])



    .config(function stateConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('dashboard', {
                abstract: true,
                url: '/dashboard',
                views: {
                    "master": {
                        templateUrl: '_dashboard.html'
                    },
                    "navbar@dashboard": {
                        templateUrl: '_navbar.html'
                    }
                }
            })
            .state('dashboard.about', {
                url: '/about',
                views: {
                    "content@dashboard": {
                        templateUrl: '_contentAbout.html'
                    }
                }
            })
            .state('dashboard.faq', {
                url: '/faq',
                views: {
                    "content@dashboard": {
                        templateUrl: '_contentFaq.html'
                    }
                },
                controller: 'ContentCtrl'
            })
            .state('dashboard.news', {
                url: '/news',
                views: {
                    "content@dashboard": {
                        controller: 'NewsCtrl',
                        templateUrl: '_news.html'
                    },
                    "navbar@dashboard": {
                        templateUrl: '_navbar.html'
                    }
                }
            })
            .state('news-detail', {
                
                url: 'dashboard/news/{nId}',
                views: {
                    "": {
                        controller: 'ModalCtrl'
                    }
                }
            })
    })

    

    .controller('AppCtrl', function HomeCtrl($scope, $state) {
        $state.go('dashboard.about')
    })

;


angular.module('content', ['ui.bootstrap', 'ui.router'])

    .controller('ContentCtrl', ['$scope', 'getJson', function($scope, getJson){
        getJson.all().then(function(resp){
            $scope.questions = resp.data.questions;
            $scope.status = {
                isItemOpen: new Array($scope.questions.length),
                isFirstDisabled: false
            };
            $scope.status.isItemOpen[0] = true;
        });
    }])

;


angular.module('news', ['ui.router'])
    .controller('NewsCtrl', ['$scope', 'getJson', '$modal', '$log', 'utils', '$stateParams', function($scope, getJson, $modal, $log, utils, $stateParams){
        getJson.all().then(function(resp){
            $scope.news = resp.data.news;

        });

        $scope.animationsEnabled = true;

        $scope.open = function () {

            var modalInstance = $modal.open({
              animation: $scope.animationsEnabled,
              templateUrl: '_newsPopup.html',
              controller: 'ModalCtrl'
            });
        };

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
    }])

    .controller('ModalCtrl', ['$scope', 'getJson', 'utils', '$stateParams', function($scope, getJson, utils, $stateParams){
        getJson.all().then(function(resp){
            $scope.news = resp.data.news;
            setTimeout(function() {
                $scope.n = utils.findById($scope.news, $stateParams.nId);
            });
            
        });
    }])
;
var gulp          = require('gulp'),
    connect       = require('gulp-connect-multi')();




gulp.task('connect', connect.server({
  root: ['../testAngular'],
  port: 3000,
  livereload: true,
  open: {
    browser: 'Google Chrome' // if not working OS X browser: 'Google Chrome' 
  }
}));


gulp.task('html', function () {
  gulp.src('*.html')
    .pipe(connect.reload());
});


gulp.task('js', function () {
  gulp.src('*.js')
    .pipe(connect.reload());
});


gulp.task('watch', function(){
    gulp.watch(['css/stylus/*.styl'], ['stylus']);
    gulp.watch(['*.html'], ['html']);
    gulp.watch(['*.js'], ['js']);
});




gulp.task('default', ['connect',  'watch']);

